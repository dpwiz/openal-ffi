# openal-ffi

> Very low-level bindings to the [OpenAL] library.

- No OpenGL dependencies.
- No point/vector/tensor/spaces dependencies.
- No state and no resource management.
- Modest DLL size.

## Examples

- [C API overview](https://ffainelli.github.io/openal-example/)
- [Buffer-playing example](https://gitlab.com/dpwiz/openal-ffi/-/blob/main/test/Spec.hs)
- [Opus-playing example](https://gitlab.com/dpwiz/opusfile/-/blob/master/examples/openal-playfile/Main.hs)

[OpenAL]: https://openal.org/

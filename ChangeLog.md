# Changelog for openal-ffi

## [0.0.1]

- Initial import.

[0.0.1]: https://gitlab.com/dpwiz/openal-ffi/-/tree/v0.0.1

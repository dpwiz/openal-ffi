module Sound.OpenAL.FFI.Utils
  ( Dynamic
  , peekCStrings
  ) where

import Foreign (FunPtr, lengthArray0, plusPtr)
import Foreign.C.String (CString, peekCAString)

type Dynamic a = FunPtr a -> a

peekCStrings :: CString -> IO [String]
peekCStrings = go []
  where
    go acc ptr = do
      str <- peekCAString ptr
      if null str then
        pure $ reverse acc
      else do
        len <- lengthArray0 0 ptr
        go (str : acc) $ plusPtr ptr (len + 1)

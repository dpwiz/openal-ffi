{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module Sound.OpenAL.FFI.AL where

import Foreign (FunPtr, Ptr, Storable)
import Foreign.C.Types
import Foreign.C.String (CString)

import Sound.OpenAL.FFI.Utils (Dynamic)

-- * Source

newtype Source = Source CUInt
  deriving stock (Eq, Ord, Show)
  deriving newtype (Storable)

foreign import ccall unsafe "alGenSources"
  alGenSources :: CInt -> Ptr Source -> IO ()

foreign import ccall unsafe "alDeleteSources"
  alDeleteSources :: CInt -> Ptr Source -> IO ()

foreign import ccall unsafe "alIsSource"
  alIsSource :: Source -> IO CChar

foreign import ccall unsafe "alGetSourcefv"
  alGetSourcefv :: Source -> CInt -> Ptr CFloat -> IO ()

foreign import ccall unsafe "alSourcefv"
  alSourcefv :: Source -> CInt -> Ptr CFloat -> IO ()

foreign import ccall unsafe "alGetSourceiv"
  alGetSourceiv :: Source ->  CInt -> Ptr CInt -> IO ()

foreign import ccall unsafe "alSourcei"
  alSourcei :: Source -> CInt -> CInt -> IO ()

foreign import ccall unsafe "alSourceQueueBuffers"
  alSourceQueueBuffers :: Source -> CInt -> Ptr Buffer -> IO ()

foreign import ccall unsafe "alSourceUnqueueBuffers"
  alSourceUnqueueBuffers :: Source -> CInt -> Ptr Buffer -> IO ()

foreign import ccall unsafe "alSourcePlayv"
  alSourcePlayv :: CInt -> Ptr Source -> IO ()

foreign import ccall unsafe "alSourcePausev"
  alSourcePausev :: CInt -> Ptr Source -> IO ()

foreign import ccall unsafe "alSourceStopv"
  alSourceStopv :: CInt -> Ptr Source -> IO ()

foreign import ccall unsafe "alSourceRewindv"
  alSourceRewindv :: CInt -> Ptr Source -> IO ()

-- * Buffer

newtype Buffer = Buffer CUInt
  deriving stock (Eq, Ord, Show)
  deriving newtype (Storable)

foreign import ccall unsafe "alGenBuffers"
  alGenBuffers :: CInt -> Ptr Buffer -> IO ()

foreign import ccall unsafe "alDeleteBuffers"
  alDeleteBuffers :: CInt -> Ptr Buffer -> IO ()

foreign import ccall unsafe "alBufferData"
  alBufferData :: Buffer -> CInt -> Ptr () -> CInt -> CInt -> IO ()

foreign import ccall unsafe "alGetBufferi"
  alGetBufferi :: Buffer -> CInt -> Ptr CInt -> IO ()

foreign import ccall unsafe "alIsBuffer"
  alIsBuffer :: Buffer -> IO CChar

-- * Errors

foreign import ccall unsafe "alGetError"
  alGetError :: IO CInt

pattern INVALID_NAME :: (Eq a, Num a) => a
pattern INVALID_NAME = 0xA001

pattern INVALID_ENUM :: (Eq a, Num a) => a
pattern INVALID_ENUM = 0xA002

pattern INVALID_VALUE :: (Eq a, Num a) => a
pattern INVALID_VALUE = 0xA003

pattern INVALID_OPERATION :: (Eq a, Num a) => a
pattern INVALID_OPERATION = 0xA004

pattern OUT_OF_MEMORY :: (Eq a, Num a) => a
pattern OUT_OF_MEMORY = 0xA005

-- * Listener

foreign import ccall unsafe "alListenerf"
  alListenerf :: CInt -> CFloat -> IO ()

foreign import ccall unsafe "alListenerfv"
  alListenerfv :: CInt -> Ptr () -> IO ()

foreign import ccall unsafe "alGetListenerfv"
  alGetListenerfv :: CInt -> Ptr CFloat -> IO ()

-- * Queries

foreign import ccall unsafe "alGetEnumValue"
  alGetEnumValue :: CString -> IO CInt

foreign import ccall unsafe "alGetFloat"
  alGetFloat :: CInt -> IO CFloat

foreign import ccall unsafe "alGetIntegerv"
  alGetIntegerv :: CInt -> Ptr CInt -> IO ()

foreign import ccall unsafe "alDistanceModel"
  alDistanceModel :: CInt -> IO ()

-- ** Strings

foreign import ccall unsafe "alGetString"
  alGetString :: CInt -> IO CString

pattern VERSION :: (Eq a, Num a) => a
pattern VERSION = 0xB002

pattern RENDERER :: (Eq a, Num a) => a
pattern RENDERER = 0xB003

pattern VENDOR :: (Eq a, Num a) => a
pattern VENDOR = 0xB001

pattern EXTENSIONS :: (Eq a, Num a) => a
pattern EXTENSIONS = 0xB004

-- ** Extensions

foreign import ccall unsafe "alIsExtensionPresent"
  alIsExtensionPresent_ :: CString -> IO CBool

foreign import ccall unsafe "alGetProcAddress"
  alGetProcAddress :: CString -> IO (FunPtr a)

foreign import ccall unsafe "dynamic"
  invokeWithFloat :: Dynamic (CFloat -> IO ())

-- * Enums

pattern DISTANCE_MODEL :: (Eq a, Num a) => a
pattern DISTANCE_MODEL = 0xD000

pattern DOPPLER_FACTOR :: (Eq a, Num a) => a
pattern DOPPLER_FACTOR = 0xC000

pattern SPEED_OF_SOUND :: (Eq a, Num a) => a
pattern SPEED_OF_SOUND = 0xC003


pattern INVERSE_DISTANCE :: (Eq a, Num a) => a
pattern INVERSE_DISTANCE = 0xD001

pattern INVERSE_DISTANCE_CLAMPED :: (Eq a, Num a) => a
pattern INVERSE_DISTANCE_CLAMPED = 0xD002

pattern LINEAR_DISTANCE :: (Eq a, Num a) => a
pattern LINEAR_DISTANCE = 0xD003

pattern LINEAR_DISTANCE_CLAMPED :: (Eq a, Num a) => a
pattern LINEAR_DISTANCE_CLAMPED = 0xD004

pattern EXPONENT_DISTANCE :: (Eq a, Num a) => a
pattern EXPONENT_DISTANCE = 0xD005

pattern EXPONENT_DISTANCE_CLAMPED :: (Eq a, Num a) => a
pattern EXPONENT_DISTANCE_CLAMPED = 0xD006


pattern POSITION :: (Eq a, Num a) => a
pattern POSITION = 0x1004

pattern VELOCITY :: (Eq a, Num a) => a
pattern VELOCITY = 0x1006

pattern GAIN :: (Eq a, Num a) => a
pattern GAIN = 0x100A


pattern ORIENTATION :: (Eq a, Num a) => a
pattern ORIENTATION = 0x100F


pattern SOURCE_RELATIVE :: (Eq a, Num a) => a
pattern SOURCE_RELATIVE = 0x0202

pattern SOURCE_TYPE :: (Eq a, Num a) => a
pattern SOURCE_TYPE = 0x1027

pattern LOOPING :: (Eq a, Num a) => a
pattern LOOPING = 0x1007

pattern BUFFER :: (Eq a, Num a) => a
pattern BUFFER = 0x1009

pattern BUFFERS_QUEUED :: (Eq a, Num a) => a
pattern BUFFERS_QUEUED = 0x1015

pattern BUFFERS_PROCESSED :: (Eq a, Num a) => a
pattern BUFFERS_PROCESSED = 0x1016

pattern MIN_GAIN :: (Eq a, Num a) => a
pattern MIN_GAIN = 0x100D

pattern MAX_GAIN :: (Eq a, Num a) => a
pattern MAX_GAIN = 0x100E

pattern REFERENCE_DISTANCE :: (Eq a, Num a) => a
pattern REFERENCE_DISTANCE = 0x1020

pattern ROLLOFF_FACTOR :: (Eq a, Num a) => a
pattern ROLLOFF_FACTOR = 0x1021

pattern MAX_DISTANCE :: (Eq a, Num a) => a
pattern MAX_DISTANCE = 0x1023

pattern PITCH :: (Eq a, Num a) => a
pattern PITCH = 0x1003

pattern DIRECTION :: (Eq a, Num a) => a
pattern DIRECTION = 0x1005

pattern CONE_INNER_ANGLE :: (Eq a, Num a) => a
pattern CONE_INNER_ANGLE = 0x1001

pattern CONE_OUTER_ANGLE :: (Eq a, Num a) => a
pattern CONE_OUTER_ANGLE = 0x1002

pattern CONE_OUTER_GAIN :: (Eq a, Num a) => a
pattern CONE_OUTER_GAIN = 0x1022

pattern SEC_OFFSET :: (Eq a, Num a) => a
pattern SEC_OFFSET = 0x1024

pattern SAMPLE_OFFSET :: (Eq a, Num a) => a
pattern SAMPLE_OFFSET = 0x1025

pattern BYTE_OFFSET :: (Eq a, Num a) => a
pattern BYTE_OFFSET = 0x1026

pattern SOURCE_STATE :: (Eq a, Num a) => a
pattern SOURCE_STATE = 0x1010


pattern UNDETERMINED :: (Eq a, Num a) => a
pattern UNDETERMINED = 0x1030

pattern STATIC :: (Eq a, Num a) => a
pattern STATIC = 0x1028

pattern STREAMING :: (Eq a, Num a) => a
pattern STREAMING = 0x1029


pattern INITIAL :: (Eq a, Num a) => a
pattern INITIAL = 0x1011

pattern PLAYING :: (Eq a, Num a) => a
pattern PLAYING = 0x1012

pattern PAUSED :: (Eq a, Num a) => a
pattern PAUSED  = 0x1013

pattern STOPPED :: (Eq a, Num a) => a
pattern STOPPED = 0x1014


pattern FREQUENCY :: (Eq a, Num a) => a
pattern FREQUENCY = 0x2001

pattern SIZE :: (Eq a, Num a) => a
pattern SIZE = 0x2004

pattern BITS :: (Eq a, Num a) => a
pattern BITS = 0x2002

pattern CHANNELS :: (Eq a, Num a) => a
pattern CHANNELS = 0x2003


pattern FORMAT_MONO8 :: (Eq a, Num a) => a
pattern FORMAT_MONO8 = 0x1100

pattern FORMAT_MONO16 :: (Eq a, Num a) => a
pattern FORMAT_MONO16 = 0x1101

pattern FORMAT_STEREO8 :: (Eq a, Num a) => a
pattern FORMAT_STEREO8 = 0x1102

pattern FORMAT_STEREO16 :: (Eq a, Num a) => a
pattern FORMAT_STEREO16 = 0x1103

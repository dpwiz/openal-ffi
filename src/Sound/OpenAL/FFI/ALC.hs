{-# LANGUAGE PatternSynonyms #-}

module Sound.OpenAL.FFI.ALC where

import Foreign (FunPtr, Ptr, nullPtr)
import Foreign.C.Types (CChar(..), CInt(..), CUInt(..))
import Foreign.C.String (CString)
import Sound.OpenAL.FFI.Utils (Dynamic)

-- * Context

newtype Context = Context (Ptr Context)
  deriving (Eq, Ord, Show)

foreign import ccall unsafe "alcCreateContext"
  alcCreateContext :: Device -> Ptr CInt -> IO Context

foreign import ccall unsafe "alcDestroyContext"
  alcDestroyContext :: Context -> IO ()

foreign import ccall unsafe "alcGetCurrentContext"
  alcGetCurrentContext :: IO Context

foreign import ccall unsafe "alcMakeContextCurrent"
  alcMakeContextCurrent :: Context -> IO CChar

foreign import ccall unsafe "alcSuspendContext"
  alcSuspendContext :: Context -> IO ()

foreign import ccall unsafe "alcProcessContext"
  alcProcessContext :: Context -> IO ()

foreign import ccall unsafe "alcGetContextsDevice"
  alcGetContextsDevice :: Context -> IO Device

-- * Device

newtype Device = Device (Ptr Device)
  deriving (Eq, Ord, Show)

nullDevice :: Device
nullDevice = Device nullPtr

foreign import ccall unsafe "alcOpenDevice"
  alcOpenDevice :: CString -> IO Device

foreign import ccall unsafe "alcCloseDevice"
  alcCloseDevice :: Device -> IO CChar

-- * Errors

foreign import ccall unsafe "alcGetError"
  alcGetError :: Device -> IO CInt

pattern INVALID_DEVICE :: (Eq a, Num a) => a
pattern INVALID_DEVICE = 0xA001

pattern INVALID_CONTEXT :: (Eq a, Num a) => a
pattern INVALID_CONTEXT = 0xA002

pattern INVALID_ENUM :: (Eq a, Num a) => a
pattern INVALID_ENUM = 0xA003

pattern INVALID_VALUE :: (Eq a, Num a) => a
pattern INVALID_VALUE = 0xA004

pattern INVALID_OPERATION :: (Eq a, Num a) => a
pattern INVALID_OPERATION = 0xA006

pattern OUT_OF_MEMORY :: (Eq a, Num a) => a
pattern OUT_OF_MEMORY = 0xA005

-- * Extensions

foreign import ccall unsafe "alcGetProcAddress"
  alcGetProcAddress :: Device -> CString -> IO (FunPtr a)

foreign import ccall unsafe "alcGetEnumValue"
  alcGetEnumValue :: Device -> CString -> IO CInt

-- * Queries

foreign import ccall unsafe "alcGetString"
  alcGetString :: Device -> CInt -> IO CString

foreign import ccall unsafe "alcGetIntegerv"
  alcGetIntegerv :: Device -> CInt -> CInt -> Ptr CInt -> IO ()

foreign import ccall unsafe "alcIsExtensionPresent"
  alcIsExtensionPresent_ :: Device -> CString -> IO CChar

-- * Capture

foreign import ccall unsafe "dynamic"
  invokeCaptureOpenDevice :: Dynamic (CString -> CUInt -> CInt -> CInt -> IO Device)

foreign import ccall unsafe "dynamic"
  invokeCaptureStartStop :: Dynamic (Device -> IO ())

foreign import ccall unsafe "dynamic"
  invokeCaptureSamples :: Dynamic (Device -> Ptr a -> CInt -> IO ())

foreign import ccall unsafe "dynamic"
  invokeCaptureCloseDevice :: Dynamic (Device -> IO CChar)

-- * Enums

pattern FREQUENCY :: (Eq a, Num a) => a
pattern FREQUENCY = 0x1007

pattern REFRESH :: (Eq a, Num a) => a
pattern REFRESH = 0x1008

pattern SYNC :: (Eq a, Num a) => a
pattern SYNC = 0x1009

pattern MONO_SOURCES :: (Eq a, Num a) => a
pattern MONO_SOURCES = 0x1010

pattern STEREO_SOURCES :: (Eq a, Num a) => a
pattern STEREO_SOURCES = 0x1011

pattern DEFAULT_DEVICE_SPECIFIER :: (Eq a, Num a) => a
pattern DEFAULT_DEVICE_SPECIFIER = 0x1004

pattern DEVICE_SPECIFIER :: (Eq a, Num a) => a
pattern DEVICE_SPECIFIER = 0x1005

pattern EXTENSIONS :: (Eq a, Num a) => a
pattern EXTENSIONS = 0x1006

pattern CAPTURE_DEFAULT_DEVICE_SPECIFIER :: (Eq a, Num a) => a
pattern CAPTURE_DEFAULT_DEVICE_SPECIFIER = 0x0311

pattern CAPTURE_DEVICE_SPECIFIER :: (Eq a, Num a) => a
pattern CAPTURE_DEVICE_SPECIFIER = 0x0310

pattern ATTRIBUTES_SIZE :: (Eq a, Num a) => a
pattern ATTRIBUTES_SIZE = 0x1002

pattern ALL_ATTRIBUTES :: (Eq a, Num a) => a
pattern ALL_ATTRIBUTES = 0x1003

pattern MAJOR_VERSION :: (Eq a, Num a) => a
pattern MAJOR_VERSION = 0x1000

pattern MINOR_VERSION :: (Eq a, Num a) => a
pattern MINOR_VERSION = 0x1001

pattern CAPTURE_SAMPLES :: (Eq a, Num a) => a
pattern CAPTURE_SAMPLES = 0x0312
